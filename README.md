# **PixelFood** 🍎

<img align="right" src="icon.png">

Sixty-four pixel-perfect food and drink sprites.

- 📦 <http://henrysoftware.itch.io/godot-pixel-food>
- 🌐 <http://rakkarage.github.io/PixelFood>
- 📃 <http://guthub.com/rakkarage/PixelFood>

[![.github/workflows/compress.yml](https://github.com/rakkarage/PixelFood/actions/workflows/compress.yml/badge.svg)](https://github.com/rakkarage/PixelFood/actions/workflows/compress.yml)
[![.github/workflows/deploy.yml](https://github.com/rakkarage/PixelFood/actions/workflows/deploy.yml/badge.svg)](https://github.com/rakkarage/PixelFood/actions/workflows/deploy.yml)
